import { Component, OnInit } from '@angular/core';

import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class AboutComponent implements OnInit {

  leader: Leader;
  leaders: Leader[];
  dishErrMess: string;

  constructor( private leaderservice: LeaderService) { }

  ngOnInit() {
    this.leaderservice.getFeaturedLeader()
    .subscribe(leaders => this.leader = leaders,
      errmess => this.dishErrMess = <any>errmess);
  }

}
