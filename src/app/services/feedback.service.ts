import { Injectable } from '@angular/core';
import { Feedback } from '../shared/feedback';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { RestangularModule, Restangular } from 'ngx-restangular';

import { baseURL } from '../shared/baseurl';


@Injectable()
export class FeedbackService {

  feedbackRest = null;
  feedback: Feedback;
  constructor(private restangular: Restangular) { }

  submitFeedback(feedback: Feedback): Observable<Feedback>{
        return this.restangular.all('feedback').post(feedback);
  }
  
  

}

